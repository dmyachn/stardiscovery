package com.example.jarvis.testone;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.spaceavoider.spaceavoider.R;


public class MainActivity extends Activity implements View.OnTouchListener {
    public static boolean isLeftPressed = false; // нажата левая кнопка
    public static boolean isRightPressed = false; // нажата правая кнопка
    static MainActivity activityA;

    public static MainActivity getInstance() {
        return activityA;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        GameView gameView = new GameView(this); // создаём gameView
        LinearLayout gameLayout = findViewById(R.id.gameLayout); // находим gameLayout
        gameLayout.addView(gameView); // и добавляем в него gameView
        TextView text1 = findViewById(R.id.gameScoreL);
        activityA = this;
        Button leftButton = findViewById(R.id.leftButton); // находим кнопки
        Button rightButton = findViewById(R.id.rightButton);
        //Mda();
        leftButton.setOnTouchListener(this); // и добавляем этот класс как слушателя (при нажатии сработает onTouch)
        rightButton.setOnTouchListener(this);

    }

    public void Mda(final int iscore) {
        if (iscore > 0) {
            runOnUiThread(new Runnable() {

                              @Override
                              public void run() {
                                  TextView text1 = findViewById(R.id.gameScoreL);
                                  text1.setText(String.valueOf(iscore));
                              }
                          }

            );
        }
    }




    public boolean onTouch(View button, MotionEvent motion) {

        switch (button.getId()) { // определяем какая кнопка
            case R.id.leftButton:
                switch (motion.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isLeftPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isLeftPressed = false;
                        break;
                }
                break;
            case R.id.rightButton:
                switch (motion.getAction()) { // определяем нажата или отпущена
                    case MotionEvent.ACTION_DOWN:
                        isRightPressed = true;
                        break;
                    case MotionEvent.ACTION_UP:
                        isRightPressed = false;
                        break;
                }
                break;
        }
        return true;
    }
}