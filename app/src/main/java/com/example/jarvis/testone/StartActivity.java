package com.example.jarvis.testone;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.spaceavoider.spaceavoider.R;


public class StartActivity extends Activity implements OnClickListener {
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.menu);


        Button startButton = findViewById(R.id.button1);
        startButton.setOnClickListener(this);

        Button levelButton = findViewById(R.id.button2);
        levelButton.setOnClickListener(this);

        Button rulesButton = findViewById(R.id.button3);
        rulesButton.setOnClickListener(this);

        Button svetButton = findViewById(R.id.button4);
        svetButton.setOnClickListener(this);

        Button exitButton = findViewById(R.id.button5);
        exitButton.setOnClickListener(this);
    }

    /** Обработка нажатия кнопок */
    public void onClick(View v) {
        switch (v.getId()) {
            //переход на сюрфейс
            case R.id.button1: {
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                startActivity(intent);
            }break;

            case R.id.button2: {
                Intent intent = new Intent();
                intent.setClass(this, Levels.class);
                startActivity(intent);
            }break;

            case R.id.button3: {
                Intent intent = new Intent();
                intent.setClass(this, Rules.class);
                startActivity(intent);
            }break;

            case R.id.button4: {
                Intent intent = new Intent();
                intent.setClass(this, About.class);
                startActivity(intent);
            }break;

            //выход
            case R.id.button5: {

                Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                homeIntent.addCategory( Intent.CATEGORY_HOME );
                homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homeIntent);
                /*finish();   finish();   finish();*/
            }break;

            default:
                break;
        }
    }
}
