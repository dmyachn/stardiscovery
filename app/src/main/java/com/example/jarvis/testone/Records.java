package com.example.jarvis.testone;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.spaceavoider.spaceavoider.R;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public class Records extends Activity implements View.OnClickListener {


    int score5;
   String name5;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.records);
      //  activityA = this;
        Button backButton = findViewById(R.id.backB);
        backButton.setOnClickListener(this);
        Button playButton = findViewById(R.id.playB);
        playButton.setOnClickListener(this);
        Mda1(GameView.score);
    }

  @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void saveString() {

      try(ObjectInputStream ois = new ObjectInputStream(new FileInputStream("saving.dat")))
      {
          Saving save=(Saving)ois.readObject();
      }
      catch(Exception ex){

          System.out.println(ex.getMessage());
      }
        EditText eT5 = findViewById(R.id.t5);
        this.name5 = eT5.getText().toString();
        this.score5 = GameView.score;

        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("saving.dat"))) {
            Saving save = new Saving(this.name5, this.score5);
            oos.writeObject(save);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void Mda1(final int iscore) {
        if (iscore > 0) {
            runOnUiThread(new Runnable() {

                              @Override
                              public void run() {

                                  TextView text1 = findViewById(R.id.s5);
                                  text1.setText(String.valueOf(iscore));
                              }
                          }

            );
        }
    }

    /**
     * Обработка нажатия кнопок
     */
    public void onClick(View v) {
        switch (v.getId()) {
            //переход на сюрфейс
            case R.id.backB: {
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    //saveString();
                }*/
                Intent intent = new Intent();
                intent.setClass(this, StartActivity.class);
                startActivity(intent);
            }
            break;

            case R.id.playB: {
               /* if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                   // saveString();
                }*/
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                startActivity(intent);
            }
            break;

            default:
                break;
        }
    }
}
