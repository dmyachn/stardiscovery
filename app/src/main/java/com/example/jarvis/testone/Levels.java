package com.example.jarvis.testone;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.spaceavoider.spaceavoider.R;


public class Levels extends Activity implements View.OnClickListener {
    static int level = 0;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.levels);


        Button easy = findViewById(R.id.easy);
       easy.setOnClickListener(this);

        Button normal = findViewById(R.id.normal);
        normal.setOnClickListener(this);

        Button hard = findViewById(R.id.hard);
        hard.setOnClickListener(this);

        Button exitButton = findViewById(R.id.buttonBack);
        exitButton.setOnClickListener(this);


    }

    /** Обработка нажатия кнопок */
    public void onClick(View v) {
        switch (v.getId()) {
            //переход на сюрфейс
            case R.id.easy: {
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                level=0;
                startActivity(intent);

            }break;

            case R.id.normal: {
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                level=1;
                startActivity(intent);
            }break;

            case R.id.hard: {
                Intent intent = new Intent();
                intent.setClass(this, MainActivity.class);
                level=2;
                startActivity(intent);
            }break;

            case R.id.buttonBack: {
                Intent intent = new Intent();
                intent.setClass(this, StartActivity.class);
                startActivity(intent);
            }break;


            default:
                break;
        }
    }
}
