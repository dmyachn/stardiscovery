package com.example.jarvis.testone;

import android.content.Context;

import static com.spaceavoider.spaceavoider.R.drawable.comet;

public class Cometa extends Asteroid {
    private float x0=x;
    private float n=0.3f;

    public Cometa(Context context) {

        super(context);
        k=(float)4.2;
        bitmapId = comet;
        init(context);

    }

    @Override
    public void update() {
        y += speed;
        x+=n*speed;
        if (x0+12*speed<x || x0-12*speed>x)
            n*=-1;
    }
}
