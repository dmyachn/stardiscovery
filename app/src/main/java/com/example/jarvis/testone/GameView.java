package com.example.jarvis.testone;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.spaceavoider.spaceavoider.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;


public class GameView extends SurfaceView implements Runnable {

    private int OBJECT_INTERVAL = 80; // время через которое появляются emoji (в итерациях)
    private int LAZER_INTERVAL = 60; // время через которое появляются emoji (в итерациях)
    public static int maxX = 20; // размер по горизонтали
    public static int maxY = 28; // размер по вертикали
    public static float unitW = 0; // пикселей в юните по горизонтали
    public static float unitH = 0; // пикселей в юните по вертикали
    public static int score;
    private boolean firstTime = true;
    private boolean gameRunning = true;
    private Ship ship;
    // private Thread scoreThread = null;
    private Thread gameThread = null;
    private Paint paint;
    private Paint mPaint = new Paint();
    private Canvas canvas;
    private Bitmap mBitmap;
    private SurfaceHolder surfaceHolder;
    private ArrayList<SpaceBody> spaceObjects = new ArrayList<>(); // тут будут харанится emoji астероиды
    private ArrayList<Lazer> lazers = new ArrayList<>(); // тут будут харанится emoji астероиды
    private int currentTimeForObject = 0;
    private int currentTimeForLazer = 0;
    // private ArrayList<Food> foods = new ArrayList<>(); // тут будут харанится шавухи
    // private ArrayList<Drink> drinks = new ArrayList<>(); // тут будут харанится старбаксы
    Scoring iscore = new Scoring();
    int level;

    protected Context mContext;

    public GameView(Context context) {

        super(context);
        this.mContext = context;
        surfaceHolder = getHolder();
        paint = new Paint();
        Resources res = this.getResources();
        mBitmap = BitmapFactory.decodeResource(res, R.drawable.background);
        gameThread = new Thread(this);
        gameThread.start();
        score = 0;
    }



    @Override
    public void run() {
        while (gameRunning) {
            if (score>100)
            {
                OBJECT_INTERVAL = 60;
                LAZER_INTERVAL = 50;
                if(score>200)
                {
                    OBJECT_INTERVAL = 52;
                    LAZER_INTERVAL = 40;
                    if (score>300)
                    {
                        OBJECT_INTERVAL = 40;
                        LAZER_INTERVAL = 30;

                        if (score>400)
                        {
                            OBJECT_INTERVAL = 30;
                            LAZER_INTERVAL = 20;
                        }
                    }
                }
            }
            update();
            draw();
            checkCollision();
            createNewObjects();
            control();
        }

    }


    private void update() {
        if (!firstTime) {
            ship.update();
            for (SpaceBody spaceObject : spaceObjects) {
                spaceObject.update();
            }
            for (Lazer lazer : lazers) {
                lazer.update();
            }
        }
    }

    private void draw() {
        if (surfaceHolder.getSurface().isValid()) {  //проверяем валидный ли surface

            if (firstTime) { // инициализация при первом запуске
                firstTime = false;
                Level();
                unitW = surfaceHolder.getSurfaceFrame().width() / maxX; // вычисляем число пикселей в юните
                unitH = surfaceHolder.getSurfaceFrame().height() / maxY;
                ship = new Ship(getContext()); // добавляем корабль
            }

            canvas = surfaceHolder.lockCanvas(); // закрываем canvas
            canvas.drawBitmap(mBitmap, 0, 0, mPaint);


            ship.drow(paint, canvas); // рисуем корабль

            for (SpaceBody SpaceObject : spaceObjects) { // рисуем астероиды
                SpaceObject.drow(paint, canvas);
            }
            for (Lazer lazer : lazers) {
                lazer.drow(paint, canvas);
            }
            surfaceHolder.unlockCanvasAndPost(canvas); // открываем canvas
        }
    }

    private void control() { // пауза на 17 миллисекунд
        try {
            Thread.sleep(17);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void Level()
    {
        level=Levels.level;
    }

    private void checkCollision() { // перебираем все астероиды и проверяем не касается ли один из них корабля
        Iterator<SpaceBody> spaceObjectIterator = spaceObjects.iterator();//создаем итератор
        while(spaceObjectIterator.hasNext()) {//до тех пор, пока в списке есть элементы
            SpaceBody nextspaceObject = spaceObjectIterator.next();//получаем следующий элемент
            if (nextspaceObject.isCollision(ship.x, ship.y, ship.size)) {
                gameRunning = false; // останавливаем игру
                Intent intent = new Intent();
                intent.setClass(mContext, Records.class);
                mContext.startActivity(intent);
            }
            Iterator<Lazer> lazerIterator = lazers.iterator();//создаем итератор
            while(lazerIterator.hasNext()) {//до тех пор, пока в списке есть элементы

                Lazer nextlazer = lazerIterator.next();//получаем следующий элемент
                if (nextspaceObject.isCollision(nextlazer.x, nextlazer.y, nextlazer.size)) {
                    if (nextspaceObject instanceof EnemyShip){
                        score += 20;
                        iscore.scoreIt(score);
                    }
                    else{
                        score += 10;
                        iscore.scoreIt(score);
                    }
                    lazerIterator.remove();//удаляем
                    spaceObjectIterator.remove();//удаляем
                }
            }
        }
    }

    private void createNewObjects(){
        createNewSpaceObjects();
        createNewLazers();
    }

    private void createNewLazers() {
        if (currentTimeForLazer >= LAZER_INTERVAL) {
            Lazer newLazer = ship.shoot(getContext());
            switch (level) {
                case 0:
                    newLazer.speed=(float) 0.2;
                    break;
                case 1:
                    newLazer.speed=(float) 0.3;
                    break;
                case 2:
                    newLazer.speed=(float) 0.4;
                    break;
            }
            lazers.add(newLazer);
            currentTimeForLazer = 0;
        } else {
            currentTimeForLazer++;
        }
    }


    private void createNewSpaceObjects() { // каждые 50 итераций добавляем новый астероид
        if (currentTimeForObject >= OBJECT_INTERVAL) {
            float minSpeed;
            float maxSpeed;
            Random random = new Random();
            SpaceBody spaceObject;
            switch (random.nextInt(5)){
                case 0:
                case 1:
                    spaceObject = new Cometa(getContext());
                    break;
                case 2:
                case 3:
                    spaceObject = new Meteor(getContext());
                    break;
                default:
                    spaceObject = new EnemyShip(getContext());
                    break;
            }
            switch (level) {
                case 0:
                    minSpeed = (float) 0.1;
                    maxSpeed = (float) 0.2;
                    spaceObject.speed = minSpeed + (maxSpeed - minSpeed) * random.nextFloat();
                    break;
                case 1:
                    minSpeed = (float) 0.2;
                    maxSpeed = (float) 0.4;
                    spaceObject.speed = minSpeed + (maxSpeed - minSpeed) * random.nextFloat();
                    break;
                case 2:
                    minSpeed = (float) 0.4;
                    maxSpeed = (float) 0.5;
                    spaceObject.speed = minSpeed + (maxSpeed - minSpeed) * random.nextFloat();
                    break;
            }
            spaceObjects.add(spaceObject);
            currentTimeForObject = 0;
        } else {
            currentTimeForObject++;
        }
    }
}