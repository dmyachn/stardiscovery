package com.example.jarvis.testone;

import android.content.Context;

import com.spaceavoider.spaceavoider.R;


public class Meteor extends Asteroid {

    public Meteor(Context context) {
        super(context);
        bitmapId = R.drawable.meteor;
        init(context);
    }

}