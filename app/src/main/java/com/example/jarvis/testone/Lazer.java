package com.example.jarvis.testone;

import android.content.Context;

public class Lazer extends SpaceBody {

    public Lazer(Context context, Ship ship) {
        bitmapId = ship.bitmapLaserId;
        x=ship.x;
        y=ship.y;
        size=2;
        init(context);
    }

    @Override
    void update() {
        y -= speed;
    }
}
