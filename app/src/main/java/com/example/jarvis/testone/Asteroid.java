package com.example.jarvis.testone;

import android.content.Context;


import com.spaceavoider.spaceavoider.R;

import java.util.Random;

public class Asteroid extends SpaceBody {
    private int radius = 2; // радиус
    public float minSpeed = (float) 0.1; // минимальная скорость
    public float maxSpeed = (float) 0.3; // максимальная скорость

    public Asteroid(Context context) {
        Random random = new Random();

        bitmapId = R.drawable.asteroid1;
        y=0;
        x = random.nextInt(GameView.maxX) - radius;
        size = 3;
        speed = minSpeed + (maxSpeed - minSpeed) * random.nextFloat();

        init(context);
    }

    @Override
    public void update() {
        y += speed;
    }

    @Override
    public boolean isCollision(float shipX, float shipY, float shipSize) {

        double d = Math.sqrt(Math.pow((shipX+shipSize/2-x-size/2), 2) + Math.pow((shipY+shipSize/2-y-size/2), 2));
        return !((d>(size/Math.sqrt(2)+1.8))|| ((x+size) < shipX)||(x > (shipX+shipSize))||((y+size) < shipY)||(y > (shipY+shipSize)));
        //((x+size) < shipX)||(x > (shipX+shipSize))||((y+size) < shipY)||(y > (shipY+shipSize))
    }
}
