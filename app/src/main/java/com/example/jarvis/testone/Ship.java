package com.example.jarvis.testone;

import android.content.Context;

import com.spaceavoider.spaceavoider.R;

public class Ship extends SpaceBody {
    protected int bitmapLaserId;
    public Ship(Context context) {
        bitmapId = R.drawable.ship; // определяем начальные параметры
        bitmapLaserId = R.drawable.lazer; // определяем начальные параметры
        size = 4;
        x=8;
        y=GameView.maxY - size - 1;
        speed = (float) 0.2;
        init(context); // инициализируем корабль
    }


    @Override
    public void update() { // перемещаем корабль в зависимости от нажатой кнопки
        if(MainActivity.isLeftPressed && x >= 0){
            x -= speed;
        }
        if(MainActivity.isRightPressed && x <= GameView.maxX - 4){
            x += speed;
        }
    }

    public Lazer shoot(Context contex){
        return (new Lazer(contex,this));
    }
}
